#1/use/bin/env bash

yarn jetify

if [[ ! "$CI" = true ]]; then
  # Do not install iOS Pods on CI
  if [[ $(sysctl -n machdep.cpu.brand_string) =~ "Apple" ]]; then
    echo "M1"
    cd ./ios && arch -x86_64 pod install && cd ..
  else
    echo "Intel"
    cd ./ios && pod install && cd ..
  fi
fi
