import {updateObject} from '../../utilities/utils'
import {REHYDRATE} from 'redux-persist'
import * as act from '../actions/actionCreator'
import {SET_USER_LOCATION} from '../actions/user'

const initialState = {
  showGlobalIndicator: false,
  galleries: [],
  homeMenus: [],
  drawerMenus: [],
  drawerFrontMenus: [],
  tabMenus: [],
  isAppSetup: true,
  isMaintenance: false,
  appState: 'SplashScreen',
  internetState: true,
  userLocation: {
    permission: 'not_allowed'
  },
  showBarCode: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case act.GET_APPSETTINGS:
      return updateObject(state, {
        galleries: action.galleries,
        homeMenus: action.homeMenus,
        drawerMenus: action.drawerMenus,
        drawerFrontMenus: action.drawerFrontMenus,
        tabMenus: action.tabMenus,
        isAppSetup: action.isAppSetup,
        welcomeInstruction: action.welcomeInstruction,
        communityImpact: action.communityImpact,
        emailNotAllowedDomains: action.emailNotAllowedDomains,
        memberDefaultTierDetail: action.memberDefaultTierDetail,
        memberDefaultTierOption: action.memberDefaultTierOption,
        sideMenuShowProfile: action.sideMenuShowProfile,
        useLegacyDesign: action.useLegacyDesign,
        sideMenuPosition: action.sideMenuPosition,
        welcomeInstructionEnable: action.welcomeInstructionEnable,
        formInputStyle: action.formInputStyle,
        profileMenu: action.profileMenu,
        pondHopper: action.pondHopper,
        enableShowTier: action.enableShowTier,
        pondHoppersEnable: action.pondHoppersEnable,
        appFlags: action.appFlags,
        emailPendingMessage: action.emailPendingMessage
      })
    case act.SET_GLOBAL_INDICATOR_VISIBILITY:
      return updateObject(state, {showGlobalIndicator: action.visible})
    case act.SET_APP_MAINTENANCE:
      return updateObject(state, {isMaintenance: action.payload})
    case act.SET_APP_STATE:
      return updateObject(state, {appState: action.payload})
    case SET_USER_LOCATION:
      return updateObject(state, {userLocation: action.payload})
    // case act.AUTHENTICATE:
    //   return updateObject(state, {appState: 'OnBoarding'})
    case REHYDRATE:
      return updateObject(state, {
        ...(action.payload?.app || {}),
        appState: 'SplashScreen',
        showGlobalIndicator: false
      })
    case act.SET_INTERNET_STATE:
      return updateObject(state, {internetState: action.payload})
    case act.LOGOUT:
      return updateObject(state, {appState: 'Auth'})
    case act.SET_SHOW_BAR_CODE:
      return updateObject(state, {showBarCode: action.visible})
    default:
      return state
  }
}
