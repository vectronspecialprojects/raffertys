import {combineReducers} from 'redux'
import infoServicesReducer from './reducers/infoServices'
import authServicesReducer from './reducers/authServices'
import appServices from './reducers/appServices'

export default combineReducers({
  infoServices: infoServicesReducer,
  authServices: authServicesReducer,
  app: appServices
})
