import {Appearance, Dimensions, Linking, Platform, StatusBar} from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import {isIOS} from '../Themes/Metrics'
import {defaultDistance} from '../constants/constants'
import isPlainObject from 'react-redux/lib/utils/isPlainObject'

export function getStatusBarHeight(skipAndroid = false) {
  if (isIOS()) {
    return isIphoneX() ? 65 : 30
  }
  if (skipAndroid) {
    return 0
  }

  return StatusBar.currentHeight
}

export function isIphoneX() {
  const {width, height} = Dimensions.get('window')
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (height === 780 ||
      width === 780 ||
      height === 812 ||
      width === 812 ||
      height === 844 ||
      width === 844 ||
      height === 896 ||
      width === 896 ||
      height === 926 ||
      width === 926)
  )
}

export function validateEmail(email) {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

export function isEmptyValues(value) {
  return (
    value === undefined ||
    value === null ||
    (typeof value === 'object' && Object.keys(value).length === 0) ||
    (typeof value === 'string' && value.trim().length === 0)
  )
}

export function formatCardNumber(number) {
  if (!number) {
    return ''
  }
  if (typeof number === 'string') {
    return number
  }
  return number.toString().match(new RegExp('.{1,4}$|.{1,4}', 'g')).join('-')
}

export function isDarkMode() {
  return Appearance.getColorScheme() === 'dark'
}

export function urlToBlob(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest()
    xhr.onerror = reject
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        resolve(xhr.response)
      }
    }
    xhr.open('GET', url)
    xhr.responseType = 'blob' // convert type
    xhr.send()
  })
}

export const updateObject = (oldObject, updatedValues) => {
  let values = {...updatedValues}
  Object.keys(updatedValues)?.map(item => {
    if (updatedValues[item] === null || updatedValues[item] === undefined) {
      values[item] = oldObject[item]
    }
  })
  return {
    ...oldObject,
    ...values
  }
}

export function openMaps(data) {
  Linking.openURL(`https://maps.google.com/?saddr=${data.start}&daddr=${data.end}`)
}

export function getIconsName(name) {
  switch (name) {
    case 'website':
      return 'globe'
    case 'food':
      return 'utensils'
    case 'drink':
      return 'coffee'
    default:
      return name
  }
}

export function getUserLocation(callback) {
  const currentLocation = () => {
    Geolocation.getCurrentPosition(
      position => {
        callback(position.coords)
      },
      error => {
        console.log(error)
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000}
    )
  }
  if (!isIOS()) {
    currentLocation()
  } else {
    Geolocation.requestAuthorization('whenInUse').then(res => {
      if (res === 'granted') {
        currentLocation()
      }
    })
  }
}

//https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
export function getDistanceFromLatLonInKm(first, second) {
  if (!first.longitude || !first.latitude || !second.longitude || !second.latitude) {
    return defaultDistance
  }
  const R = 6371 // Radius of the earth in km
  const dLat = deg2rad(+second.latitude - +first.latitude) // deg2rad below
  const dLon = deg2rad(+second.longitude - +first.longitude)
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(first.latitude)) *
      Math.cos(deg2rad(second.latitude)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2)
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  const d = R * c // Distance in km
  return d
}

function deg2rad(deg) {
  return +deg * (Math.PI / 180)
}

export function isTrue(value) {
  return (
    value === true ||
    value === 'true' ||
    value === '1' ||
    value === 1 ||
    (typeof value === 'string' && value.length > 0 && value !== 'false')
  )
}

export function sortVenueByDistance(list, currentLocation) {
  let data = list.map(item => {
    return {
      ...item,
      distance: getDistanceFromLatLonInKm(currentLocation, item)
    }
  })
  return data.sort((a, b) => a.distance - b.distance)
}

export function getStateFromPostCode(postCode) {
  switch (postCode) {
    case 4825:
      return 'NT'
    case 872:
      return 'SA'
    case 2611:
      return 'NSW'
    case 2620:
      return 'ACT'
    case 3585:
    case 3586:
    case 3644:
    case 3691:
    case 3707:
    case 4380:
    case 4377:
    case 4383:
    case 4385:
      return 'NSW'
    default:
      if (
        (postCode >= 1000 && postCode <= 2599) ||
        (postCode >= 2619 && postCode <= 2899) ||
        (postCode >= 2921 && postCode <= 2999)
      ) {
        return 'NSW'
      } else if (
        (postCode >= 200 && postCode <= 299) ||
        (postCode >= 2600 && postCode <= 2618) ||
        (postCode >= 2900 && postCode <= 2920)
      ) {
        return 'ACT'
      } else if ((postCode >= 3000 && postCode <= 3999) || (postCode >= 8000 && postCode <= 8999)) {
        return 'VIC'
      } else if ((postCode >= 4000 && postCode <= 4999) || (postCode >= 9000 && postCode <= 9999)) {
        return 'QLD'
      } else if ((postCode >= 5000 && postCode <= 5799) || (postCode >= 5800 && postCode <= 5999)) {
        return 'SA'
      } else if ((postCode >= 6000 && postCode <= 6797) || (postCode >= 6800 && postCode <= 6999)) {
        return 'WA'
      } else if ((postCode >= 7000 && postCode <= 7799) || (postCode >= 7800 && postCode <= 7999)) {
        return 'TAS'
      } else if ((postCode >= 800 && postCode <= 899) || (postCode >= 900 && postCode <= 999)) {
        return 'NT'
      } else {
        return ''
      }
  }
}

export function convertArrayToObject(array, key) {
  if (!array?.length) {
    return {}
  }
  let obj = {}
  array?.map(item => {
    obj[item[key]] = item
  })
  return obj
}

export function parseStringToJson(string) {
  try {
    if (typeof string === 'string') {
      return JSON.parse(string)
    } else {
      return string
    }
  } catch (e) {
    return string
  }
}

const snakeToCamel = str => {
  if (str.includes('_')) {
    return str.toLowerCase().replace(/([-_][a-z])/g, group => group.slice(-1).toUpperCase())
  } else {
    return str
  }
}

export const convertObjectSnakeToCamel = object => {
  if (isPlainObject(object)) {
    const n = {}
    Object.keys(object).forEach(k => (n[snakeToCamel(k)] = convertObjectSnakeToCamel(object[k])))
    return n
  }
  // else if (isArray(object)) object.map(i => convertObjectSnakeToCamel(i))
  return object
}

export const getKeyboardVerticalOffset = () => {
  return getStatusBarHeight() + 65
}
