import Fonts from '../Themes/Fonts'
import {responsiveFont, responsiveWidth} from './Metrics'

export default {
  header: {
    fontFamily: Fonts.openSans,
    textTransform: 'uppercase',
    fontSize: responsiveFont(20),
    textAlign: 'center'
  },
  dayFrame: {
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: responsiveWidth(3),
    marginHorizontal: responsiveWidth(2)
  },
  centerContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  x2SmallNormalText: {
    fontSize: responsiveFont(10),
    fontFamily: Fonts.regular,
    textAlign: 'center'
  },
  xSmallNormalText: {
    fontSize: responsiveFont(12),
    fontFamily: Fonts.regular,
    textAlign: 'center'
  },
  xSmallUpBoldText: {
    fontSize: responsiveFont(12),
    textTransform: 'uppercase',
    fontFamily: Fonts.bold
  },
  xSmallBoldText: {
    fontSize: responsiveFont(12),
    fontFamily: Fonts.bold
  },
  xSmallUpText: {
    fontSize: responsiveFont(12),
    textTransform: 'uppercase',
    fontFamily: Fonts.regular
  },
  xSmallCapText: {
    fontSize: responsiveFont(12),
    textTransform: 'capitalize',
    fontFamily: Fonts.openSans,
    alignSelf: 'center'
  },
  smallUpBoldText: {
    fontSize: responsiveFont(14),
    textTransform: 'uppercase',
    fontFamily: Fonts.openSansBold
  },
  smallCapText: {
    fontSize: responsiveFont(14),
    textTransform: 'capitalize',
    fontFamily: Fonts.openSans,
    alignSelf: 'center'
  },
  smallCapBoldText: {
    fontSize: responsiveFont(14),
    textTransform: 'capitalize',
    fontFamily: Fonts.openSansBold,
    alignSelf: 'center'
  },
  smallNormalText: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans
  },
  mediumCapBoldText: {
    fontSize: responsiveFont(16),
    textTransform: 'capitalize',
    fontFamily: Fonts.openSansBold,
    alignSelf: 'center',
    textAlign: 'center'
  },
  mediumBoldText: {
    fontSize: responsiveFont(16),
    fontFamily: Fonts.openSansBold,
    alignSelf: 'center',
    textAlign: 'center'
  },
  mediumCapText: {
    fontSize: responsiveFont(16),
    textTransform: 'capitalize',
    fontFamily: Fonts.openSans,
    alignSelf: 'center',
    textAlign: 'center'
  },
  largeCapBoldText: {
    fontSize: responsiveFont(18),
    textTransform: 'capitalize',
    fontFamily: Fonts.openSansBold,
    alignSelf: 'center',
    textAlign: 'center'
  },
  largeCapText: {
    fontSize: responsiveFont(18),
    textTransform: 'capitalize',
    fontFamily: Fonts.openSans,
    textAlign: 'center'
  },
  largeNormalText: {
    fontSize: responsiveFont(18),
    fontFamily: Fonts.openSans
  },
  drawer: {
    paddingLeft: 20
  }
}
