import {convertObjectSnakeToCamel} from '../utilities/utils'

let colors = {
  white: '#fff',
  black: '#000',
  opacity: 'rgba(0,0,0,0.45)',
  gray: 'gray'
}

const baseColor = {
  primary: '#65c8c6',
  black: '#202224',
  white: '#ffffff'
}

const initColor = {
  backgroundFill: baseColor.black,
  backgroundText: '#202224',
  cardFill: '#25282a',
  cardText: baseColor.white,
  heroFill: baseColor.primary,
  heroText: baseColor.primary,
  errorText: baseColor.primary,
  linkText: baseColor.primary,
  activeTabFill: baseColor.primary,
  activeTabText: baseColor.white,
  inactiveTabFill: '#202224',
  inactiveTabText: baseColor.white,
  venueSelectorFill: '#202224',
  venueSelectorText: baseColor.white,
  loginButtonFill: baseColor.primary,
  loginButtonBorder: baseColor.primary,
  loginButtonText: baseColor.white,
  signupButtonFill: 'transparent',
  signupButtonBorder: baseColor.primary,
  signupButtonText: baseColor.white,
  matchAccountButtonFill: 'transparent',
  matchAccountButtonBorder: 'transparent',
  matchAccountButtonText: baseColor.primary,
  okButtonFill: baseColor.primary,
  okButtonBorder: baseColor.primary,
  okButtonText: baseColor.white,
  cancelButtonFill: 'transparent',
  cancelButtonBorder: baseColor.primary,
  cancelButtonText: baseColor.primary,
  venueTagActiveButtonFill: baseColor.primary,
  venueTagActiveButtonBorder: baseColor.primary,
  venueTagActiveButtonText: baseColor.white,
  venueTagInactiveButtonFill: 'transparent',
  venueTagInactiveButtonBorder: baseColor.primary,
  venueTagInactiveButtonText: baseColor.primary,
  formInputFieldsFill: '#25282a',
  formInputFieldsBorder: '#25282a',
  formInputFieldsText: baseColor.white,
  alertDialogFill: baseColor.white,
  alertDialogText: '#000'
}

export function setColors(obj) {
  let converted = convertObjectSnakeToCamel(obj)
  colors = {
    ...colors,
    ...initColor,
    ...converted,
    disabled: getColorOpacity(converted.formInputFieldsFill, 0.3)
  }
}

export default function Colors() {
  return colors
}

const getColorOpacity = (color, opacity) => {
  if (opacity >= 0 && opacity <= 1 && color.includes('#')) {
    const hexValue = Math.round(opacity * 255).toString(16)
    return `${color.slice(0, 7)}${hexValue.padStart(2, '0').toUpperCase()}`
  }
  return color
}
