import React from 'react'
import {Dimensions, StyleSheet, Text, View} from 'react-native'
import Colors from '../Themes/Colors'
import FastImage from 'react-native-fast-image'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import {useDispatch, useSelector} from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import PagingScrollView from '../components/PagingScrollView'
import {setAppState} from '../store/actions/appServices'
import CustomIcon from '../components/CustomIcon'
import {logoImage} from '../constants/constants'
import ScreenContainer from '../components/ScreenContainer'
import ComponentContainer from '../components/ComponentContainer'

const {width, height} = Dimensions.get('window')

function OnboardingScreen() {
  const dispatch = useDispatch()
  const welcomeInstruction = useSelector(state => state.app.welcomeInstruction)

  function handleFinish() {
    dispatch(setAppState('Main'))
    AsyncStorage.setItem('isSecondTime', 'true')
  }

  return (
    <ScreenContainer>
      <PagingScrollView
        style={{width: width}}
        total={welcomeInstruction?.length}
        finishAction={route => {
          handleFinish(route)
        }}
        showDot={true}
        hasAction={true}
      >
        {welcomeInstruction?.map((item, index) => {
          return (
            <View style={styles.itemView} key={index.toString()}>
              <FastImage source={{uri: logoImage}} style={styles.image} resizeMode={'contain'} />
              <ComponentContainer style={styles.contentContainer}>
                <CustomIcon
                  size={70}
                  image_icon={item.image_icon}
                  name={item.icon}
                  icon_selector={item.icon_selector}
                />
                <Text style={styles.title(Colors().heroText)}>{item.title}</Text>
                <View style={styles.subTitlePadding}>
                  <Text style={styles.subTitle(Colors().heroText)}>{item.content}</Text>
                </View>
              </ComponentContainer>
            </View>
          )
        })}
      </PagingScrollView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  skipButton: {
    position: 'absolute',
    top: 10,
    right: 15,
    fontSize: 15
  },
  itemView: {
    alignItems: 'center',
    width,
    height
  },
  title: color => ({
    fontSize: responsiveFont(30),
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(10),
    color
  }),
  subTitle: color => ({
    fontSize: responsiveFont(16),
    marginTop: responsiveHeight(30),
    textAlign: 'center',
    fontFamily: Fonts.openSans,
    color
  }),
  image: {
    width: responsiveWidth(200),
    height: responsiveHeight(100),
    marginBottom: responsiveHeight(40),
    marginTop: responsiveHeight(120)
  },
  bgImage: {
    width: width,
    height: responsiveHeight(373),
    paddingHorizontal: responsiveWidth(30),
    marginTop: responsiveHeight(52)
  },
  subTitlePadding: {
    paddingHorizontal: responsiveWidth(30)
  },
  contentContainer: {
    alignItems: 'center',
    paddingVertical: responsiveHeight(20),
    width: '70%'
  }
})

export default OnboardingScreen
