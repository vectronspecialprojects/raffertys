import React, {useState} from 'react'
import {View, StyleSheet, FlatList, LayoutAnimation} from 'react-native'
import PreferredVenueButton from '../../../components/PreferredVenueButton'
import Colors from '../../../Themes/Colors'
import {localize} from '../../../locale/I18nConfig'
import {responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import ButtonView from '../../../components/ButtonView'
import ItemList from '../ItemList'
import Fonts from '../../../Themes/Fonts'
import ScreenContainer from '../../../components/ScreenContainer'
import ListEmpty from '../../../components/ListEmpty'
import CustomRefreshControl from '../../../components/CustomRefreshControl'
import {isTrue} from '../../../utilities/utils'

function PreferredVenueVersion1({
  profile,
  navigation,
  venueTagsSetting,
  venueTagShow,
  isRefreshing,
  savePreferredVenue,
  loadContent,
  setSelectedVenue,
  selectedVenue
}) {
  const [selectedTag, setSelectedTag] = useState(null)

  const tagHandler = tag => {
    if (selectedTag === tag.id) {
      setSelectedTag(null)
    } else {
      setSelectedTag(tag.id)
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  const renderVenuesItem = itemData => {
    const venue = itemData.item
    const isSelected = selectedTag === venue.id
    return (
      <ItemList
        style={[
          styles.venue,
          {backgroundColor: Colors().cardFill},
          isSelected && {backgroundColor: Colors().heroFill}
        ]}
        textStyle={isSelected && {...styles.venueActiveText, color: Colors().cardText}}
        data={venue}
        isArrowShowed={false}
        onPress={() => setSelectedVenue(venue.id)}
      />
    )
  }

  const renderVenueList = data => {
    return (
      <View style={{marginTop: responsiveHeight(21)}}>
        <FlatList
          data={data}
          renderItem={renderVenuesItem}
          keyExtractor={item => item.id.toString()}
          ListEmptyComponent={<ListEmpty message={'No venues found, please check again later.'} />}
        />
      </View>
    )
  }
  const renderItem = itemData => {
    const tag = itemData.item
    const isSelected = selectedTag === tag.id
    return (
      <View>
        <ItemList
          data={tag}
          onPress={tagHandler.bind(this, tag)}
          isArrowShowed={true}
          isSelected={isSelected}
          textStyle={{color: Colors().cardText}}
        />
        {isSelected && renderVenueList(itemData.item?.listVenue)}
      </View>
    )
  }
  return (
    <ScreenContainer>
      <PreferredVenueButton
        icon="down"
        venueName={profile?.member?.current_preferred_venue_name}
        ifOnlineOrdering={profile?.member?.current_preferred_venue_full?.your_order_integration}
        onPress={() => {
          navigation.pop()
        }}
      />
      <View style={{flex: 1}}>
        {isTrue(venueTagsSetting?.isTagEnabled) ? (
          <FlatList
            data={venueTagShow}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
            ListEmptyComponent={<ListEmpty message={'No venues found, please check again later.'} />}
            refreshControl={<CustomRefreshControl refreshing={isRefreshing} onRefresh={loadContent} />}
          />
        ) : (
          renderVenueList()
        )}
      </View>
      <View style={{marginHorizontal: responsiveWidth(24), marginVertical: responsiveHeight(10)}}>
        <ButtonView title={localize('saveChanges')} onPress={savePreferredVenue} />
      </View>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  venue: {
    paddingLeft: responsiveWidth(50),
    marginTop: responsiveHeight(0),
    height: responsiveHeight(45)
  },
  venueActiveText: {
    fontFamily: Fonts.openSansBold
  }
})
export default PreferredVenueVersion1
