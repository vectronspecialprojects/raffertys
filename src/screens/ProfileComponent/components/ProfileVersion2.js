import React, {useCallback, useMemo, useRef} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import Avatar from '../../../components/Avatar'
import Fonts from '../../../Themes/Fonts'
import {localize} from '../../../locale/I18nConfig'
import BarcodeBar from '../../../components/BarcodeBar'
import SelectPreferredVenue from './SelectPreferredVenue'
import MenuItem from './MenuItem'
import RouteKey from '../../../navigation/RouteKey'
import {useDispatch, useSelector} from 'react-redux'
import {NegativeButton} from '../../../components/ButtonView'
import * as authServicesActions from '../../../store/actions/authServices'
import Alert from '../../../components/Alert'
import {BackgroundText} from '../../../components/Text'
import {isTrue} from '../../../utilities/utils'

const ACCOUNT_MENU = [
  {
    title: 'Edit my detail',
    route: RouteKey.UpdateAccountScreen
  },
  {
    title: 'Change my password',
    route: RouteKey.ResetPasswordScreen
  }, // {
  //   title: 'Change my profile picture',
  //   route: '',
  //   key: 'updateAvatar',
  // },
  {
    title: 'My favourites',
    route: RouteKey.FavoriteScreen,
    params: ''
  },
  {
    title: 'Delete App Account',
    key: 'deleteAccount'
  }
]

function ProfileVersion2({
  updateImage,
  setUpdateImage,
  profile,
  navigation,
  handleUpdateAvatar,
  handleDeleteAccount
}) {
  const profileMenu = useSelector(state => state.app.profileMenu)
  const appFlags = useSelector(state => state.app.appFlags)
  const avatarRef = useRef()
  const dispatch = useDispatch()

  const isAllowChangeAvatar = useMemo(() => {
    if (
      isTrue(profile?.member?.member_tier?.tier?.single_photo_upload) &&
      isTrue(profile?.member?.is_photo_uploaded)
    ) {
      return false
    }
    return true
  }, [profile])

  const handleLogout = useCallback(() => {
    Alert.alert('Logout', 'Are you sure you want to logout?', [
      {
        text: 'No'
      },
      {
        text: 'Yes',
        onPress: () => dispatch(authServicesActions.logout()),
        type: 'negative'
      }
    ])
  }, [dispatch])

  return (
    <View style={styles.container}>
      <View style={{height: responsiveHeight(56), backgroundColor: Colors().heroFill}}>
        <Avatar
          uri={updateImage?.path || profile?.member?.profile_img}
          size={responsiveWidth(112)}
          style={styles.avatar}
          editable={isAllowChangeAvatar}
          onImageSelect={image => {
            setUpdateImage(image)
            handleUpdateAvatar(image)
          }}
          ref={avatarRef}
          showSpinner={isTrue(appFlags?.allowed_member_photo_spin)}
        />
      </View>
      <ScrollView
        style={{marginTop: responsiveHeight(60), paddingHorizontal: responsiveWidth(20)}}
        showsVerticalScrollIndicator={false}
      >
        <BackgroundText style={styles.name}>
          {profile?.member?.first_name} {profile?.member?.last_name}
        </BackgroundText>
        <BackgroundText style={styles.tier}>{profile?.member?.member_tier?.tier?.name}</BackgroundText>
        <BarcodeBar
          value={profile?.member?.bepoz_account_card_number}
          width={responsiveWidth(profile?.member?.bepoz_account_card_number?.length <= 10 ? 1.7 : 1.2)}
          title={localize('memberNumber')}
          text={profile?.member?.bepoz_account_number}
          style={styles.barcode}
        />
        <SelectPreferredVenue color={Colors().backgroundText} />
        <BackgroundText style={styles.title}>Account</BackgroundText>
        {ACCOUNT_MENU.map((item, index) => {
          return (
            <MenuItem
              key={index.toString()}
              title={item.title}
              color={Colors().backgroundText}
              onPress={() => {
                if (item.key === 'deleteAccount') {
                  handleDeleteAccount?.()
                } else {
                  navigation.navigate(item.route)
                }
              }}
            />
          )
        })}
        <BackgroundText
          style={[
            styles.title,
            {
              marginTop: responsiveHeight(20)
            }
          ]}
        >
          {localize('profile.support')}
        </BackgroundText>
        {profileMenu?.map((item, index) => {
          return (
            <MenuItem
              key={index.toString()}
              title={item.page_name}
              color={Colors().backgroundText}
              onPress={() => {
                navigation.navigate(RouteKey.profileMenu + item.id)
              }}
            />
          )
        })}
        <NegativeButton
          style={styles.buttonSignOut}
          title={localize('profile.signOut')}
          onPress={handleLogout}
        />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  avatar: {
    alignSelf: 'center',
    bottom: responsiveWidth(-56),
    position: 'absolute',
    zIndex: 999
  },
  name: {
    fontSize: responsiveFont(20),
    textAlign: 'center',
    fontFamily: Fonts.openSansBold
  },
  tier: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    fontFamily: Fonts.openSans
  },
  barcode: {
    paddingTop: responsiveHeight(24)
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16),
    marginTop: responsiveHeight(5)
  },
  buttonSignOut: {
    marginVertical: responsiveHeight(30)
  }
})
export default ProfileVersion2
