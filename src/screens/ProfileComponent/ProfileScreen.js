import React, {useCallback, useMemo, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {updateProfile, updateProfileAvatar} from '../../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Toast from '../../components/Toast'
import {localize} from '../../locale/I18nConfig'
import ProfileVersion1 from './components/ProfileVersion1'
import ProfileVersion2 from './components/ProfileVersion2'
import ScreenContainer from '../../components/ScreenContainer'
import Alert from '../../components/Alert'
import {requestDeleteAccount} from '../../utilities/ApiManage'
import ConfirmDeleteAccount from './ConfirmDeleteAccount'

const ProfileScreen = props => {
  const profile = useSelector(state => state.infoServices.profile)
  const [dataUpdate, setDataUpdate] = useState({
    first_name: profile?.member?.first_name,
    last_name: profile?.member?.last_name
  })
  const [updateImage, setUpdateImage] = useState('')
  const dispatch = useDispatch()
  const useLegacyDesign = useSelector(state => state.app.useLegacyDesign)
  const [showInputCode, setShowInputCode] = useState(false)

  const handleChangeData = useCallback(
    (key, value) => {
      setDataUpdate({
        ...dataUpdate,
        [key]: value
      })
    },
    [dataUpdate]
  )

  async function handleUpdateData() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      if (isAllowUpdate) {
        await dispatch(updateProfile(dataUpdate, updateImage))
      }
      if (updateImage) {
        await dispatch(updateProfileAvatar(updateImage))
        setUpdateImage('')
      }
      Toast.success(localize('profile.messUpdateSuccess'))
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  async function handleUpdateAvatar(image) {
    try {
      await dispatch(updateProfileAvatar(image))
      setUpdateImage('')
      Toast.success(localize('profile.messUpdateSuccess'))
    } catch (e) {}
  }

  const isAllowUpdate = useMemo(() => {
    return (
      dataUpdate?.first_name !== profile?.member?.first_name ||
      dataUpdate?.last_name !== profile?.member?.last_name
    )
  }, [profile, dataUpdate])

  const Layout = useLegacyDesign ? ProfileVersion1 : ProfileVersion2

  const handleDeleteAccount = useCallback(() => {
    Alert.alert(localize('profile.deleteAppAccount'), localize('profile.messageConfirmDelete'), [
      {
        text: 'No'
      },
      {
        text: 'Yes',
        onPress: async () => {
          try {
            dispatch(setGlobalIndicatorVisibility(true))
            const res = await requestDeleteAccount()
            if (!res.ok) {
              throw new Error(res.message)
            }
            setShowInputCode(true)
          } catch (e) {
            Alert.alert('', e.message)
          } finally {
            dispatch(setGlobalIndicatorVisibility(false))
          }
        },
        type: 'negative'
      }
    ])
  }, [dispatch])

  const handleDismiss = useCallback(() => {
    setShowInputCode(false)
  }, [])

  return (
    <ScreenContainer>
      <Layout
        profile={profile}
        navigation={props.navigation}
        dataUpdate={dataUpdate}
        handleUpdateData={handleUpdateData}
        handleChangeData={handleChangeData}
        isAllowUpdate={isAllowUpdate}
        setUpdateImage={setUpdateImage}
        updateImage={updateImage}
        handleUpdateAvatar={handleUpdateAvatar}
        handleDeleteAccount={handleDeleteAccount}
      />
      <ConfirmDeleteAccount visible={showInputCode} onDismiss={handleDismiss} />
    </ScreenContainer>
  )
}

export default ProfileScreen
