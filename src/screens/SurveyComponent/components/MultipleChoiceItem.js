import React, {useState} from 'react'
import {View, StyleSheet} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import Colors from '../../../Themes/Colors'
import RadioButton from '../../../components/RadioButton'
import ComponentContainer from '../../../components/ComponentContainer'
import {BackgroundText} from '../../../components/Text'

function MultipleChoiceItem({onSelect, data}) {
  const [answerId, setAnswerId] = useState(data.answer_id)
  return (
    <ComponentContainer style={styles.container}>
      <View style={styles.titleWrapper}>
        <BackgroundText style={styles.title}>{data?.question}</BackgroundText>
      </View>
      {data?.answers?.map((item, index) => {
        return (
          <RadioButton
            style={{paddingVertical: responsiveHeight(5)}}
            titleStyle={{color: Colors().cardText}}
            tintColor={Colors().cardText}
            radioButtonColor={Colors().cardText}
            title={item.answer}
            onPress={() => {
              setAnswerId(item.id)
              onSelect(item.id)
            }}
            value={answerId === item.id}
          />
        )
      })}
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(20),
    borderRadius: responsiveHeight(10),
    padding: responsiveWidth(16)
  },
  title: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold
  },
  titleWrapper: {
    borderBottomWidth: 0.5,
    paddingBottom: responsiveHeight(16)
  }
})

export default MultipleChoiceItem
