import React, {useEffect, useState} from 'react'
import {StyleSheet, TouchableOpacity, TextInput} from 'react-native'
import Colors from '../../../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Fonts from '../../../Themes/Fonts'
import ComponentContainer from '../../../components/ComponentContainer'

function QuantityComponent({
  value,
  setValue,
  maxValue = 999,
  style,
  buttonStyle,
  isUpdateNow,
  minValue = 0,
  disabled
}) {
  const [inputValue, setInputValue] = useState(value)
  useEffect(() => {
    setInputValue(value)
  }, [value])
  return (
    <ComponentContainer style={[styles.container, style]}>
      {!disabled && (
        <TouchableOpacity
          style={[
            styles.buttonDecrease,
            {
              borderColor: Colors().cardFill,
              backgroundColor: Colors().heroFill
            },
            buttonStyle
          ]}
          onPress={() => {
            if (value > minValue) {
              setValue(value - 1)
            }
          }}
        >
          <MaterialIcons name={'remove'} color={Colors().backgroundFill} size={25} />
        </TouchableOpacity>
      )}
      <TextInput
        numberOfLines={1}
        maxLength={3}
        style={styles.textInput}
        placeholder={'0'}
        onChangeText={text => {
          if (+text <= maxValue) {
            if (isUpdateNow) {
              setValue(+text)
            } else {
              setInputValue(text)
            }
          }
        }}
        onBlur={() => {
          setValue(+inputValue)
        }}
        value={inputValue ? `${inputValue}` : ''}
      />
      {!disabled && (
        <TouchableOpacity
          style={[
            styles.buttonIncrease,
            {
              borderColor: Colors().cardFill,
              backgroundColor: Colors().heroFill
            },
            buttonStyle
          ]}
          onPress={() => {
            if (value < maxValue) {
              setValue(value + 1)
            }
          }}
        >
          <MaterialIcons name={'add'} color={Colors().backgroundFill} size={25} />
        </TouchableOpacity>
      )}
    </ComponentContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: responsiveHeight(36),
    alignItems: 'center',
    borderRadius: responsiveWidth(4)
  },
  buttonIncrease: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: responsiveWidth(5),
    height: '100%',
    borderRadius: responsiveWidth(4)
  },
  buttonDecrease: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: responsiveWidth(5),
    borderRadius: responsiveWidth(4)
  },
  textInput: {
    fontSize: responsiveFont(17),
    textAlign: 'center',
    textAlignVertical: 'center',
    marginBottom: 0,
    paddingBottom: 5,
    minWidth: responsiveWidth(56),
    fontFamily: Fonts.regular
  }
})

export default QuantityComponent
