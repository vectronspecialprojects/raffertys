import React, {useMemo, useEffect, useState, useCallback, useRef, useLayoutEffect} from 'react'
import {View, ActivityIndicator, StyleSheet, Platform} from 'react-native'
import {WebView} from 'react-native-webview'
import {useDispatch, useSelector} from 'react-redux'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import {checkTokenValid} from '../store/actions/authServices'
import {URL} from 'react-native-url-polyfill'
import ScreenContainer from '../components/ScreenContainer'
import {createPaymentGooglePay} from '../utilities/Stripe'
import HeaderLeftButton from '../components/HeaderLeftButton'
import {parseStringToJson} from '../utilities/utils'
import {isIOS} from '../Themes/Metrics'

function formatToken(uri, token) {
  if (uri?.includes('?')) {
    return `${uri}&id_token=${token}&myplace=${Platform.OS}`
  } else {
    return `${uri}?id_token=${token}&myplace=${Platform.OS}`
  }
}

function formatUrl(url) {
  if (url?.includes('?')) {
    return `${url}&myplace=${Platform.OS}`
  } else {
    return `${url}?myplace=${Platform.OS}`
  }
}

const BASE_YOUR_ORDER = 'https://yomyplace-'
const YouorderScreen = ({navigation, route}) => {
  const [idToken, setValidToken] = useState('')
  const dispatch = useDispatch()
  const dynamicUri = useSelector(
    state => state.infoServices.profile?.member?.current_preferred_venue_full?.your_order_link
  )
  const profile = useSelector(state => state.infoServices.profile)
  const staticUri = route.params?.params?.special_link
  const qrcodeUri = route.params?.params?.qrcodeUri
  const venues = useSelector(state => state.infoServices.venues)
  const webviewRef = useRef()
  const webviewStateRef = useRef()

  useEffect(() => {
    dispatch(
      checkTokenValid(value => {
        setValidToken(value?.id_token)
      })
    )
  }, [dispatch])

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <HeaderLeftButton
          onPress={() => {
            if (webviewStateRef.current?.canGoBack) {
              webviewRef.current?.goBack()
            } else {
              navigation.goBack()
            }
          }}
        />
      )
    })
  }, [navigation])

  const checkVenueValid = useCallback(() => {
    try {
      let valid = false
      const {venue_pivot_tags} = profile?.member.member_tier?.tier?.allowed_venues_tag || {}
      const dataVenues = profile?.member.member_tier?.tier?.allowed_venues === '0' ? venues : venue_pivot_tags
      //Case check location
      if (qrcodeUri?.includes(BASE_YOUR_ORDER)) {
        let data = new URL(qrcodeUri?.replace('#!/', ''))
        let location = data.searchParams?.get('location')
        if (location) {
          dataVenues.some(item => {
            if (location === item.your_order_location) {
              valid = true
              return true
            }
          })
        }
      } else {
        //Case check your order link
        dataVenues.some(item => {
          if (qrcodeUri?.includes(item?.your_order_link)) {
            valid = true
            return true
          }
        })
      }

      if (valid) {
        return formatToken(qrcodeUri, idToken)
      } else {
        return formatUrl(qrcodeUri)
      }
    } catch (e) {
      return formatUrl(qrcodeUri)
    }
  }, [qrcodeUri, profile, idToken, venues])

  const uri = useMemo(() => {
    // If user not confirm email return url without token
    if (!profile?.email_confirmation) {
      return formatUrl(qrcodeUri || staticUri)
    }
    if (qrcodeUri) {
      return checkVenueValid()
    }
    if (staticUri) {
      return formatToken(staticUri, idToken)
    } else if (!staticUri && dynamicUri) {
      return formatToken(dynamicUri, idToken)
    }
    return ''
  }, [qrcodeUri, staticUri, dynamicUri, checkVenueValid, idToken, profile])

  useEffect(() => {
    if (!uri) {
      Alert.alert(localize('yourOrder.notAvailable'), localize('yourOrder.message'), [
        {text: localize('okay')}
      ])
      navigation.goBack()
    }
  }, [uri, navigation])

  const handlePostMessage = useCallback(message => {
    let formatted = typeof message === 'string' ? message : JSON.stringify(message)
    console.log(formatted)
    webviewRef.current?.postMessage?.(formatted)
  }, [])

  const handleMessage = useCallback(
    event => {
      const data = parseStringToJson(event?.nativeEvent?.data)
      if (data?.data?.publishable_key) {
        handlePostMessage({message: 'received'})
        createPaymentGooglePay(data.data?.publishable_key, {
          id: data?.data?.connected_id,
          total_price: data?.data?.total_payment
        }).then(res => {
          if (res.id) {
            handlePostMessage({payment_method_id: res.id})
          } else {
            handlePostMessage(res)
          }
        })
      }
    },
    [handlePostMessage]
  )

  const onLoad = useCallback(({nativeEvent}) => {
    webviewStateRef.current = nativeEvent
  }, [])

  return (
    <ScreenContainer>
      {!!idToken && (
        <WebView
          ref={webviewRef}
          startInLoadingState={true}
          source={{uri}}
          // source={{uri: `https://google.com`}}
          enableApplePay={isIOS()}
          automaticallyAdjustContentInsets={true}
          useWebKit={true}
          renderLoading={() => (
            <View style={styles.loadingContainer}>
              <ActivityIndicator size="large" />
            </View>
          )}
          onMessage={handleMessage}
          onLoadEnd={onLoad}
          onLoadProgress={onLoad}
          onLoadStart={onLoad}
          onLoad={onLoad}
        />
      )}
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    right: '50%'
  }
})
export default YouorderScreen
