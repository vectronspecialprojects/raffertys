import React, {useCallback, useState, useMemo} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {View, FlatList} from 'react-native'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {Card} from '../../modals/modals'
import CardItem from '../../components/CardItem'
import Alert from '../../components/Alert'
import {fetchMembershipList} from '../../store/actions/infoServices'
import Colors from '../../Themes/Colors'
import SubHeaderBar from '../../components/SubHeaderBar'
import {localize} from '../../locale/I18nConfig'
import {responsiveHeight} from '../../Themes/Metrics'
import RouteKey from '../../navigation/RouteKey'
import ScreenContainer from '../../components/ScreenContainer'
import ListEmpty from '../../components/ListEmpty'
import CustomRefreshControl from '../../components/CustomRefreshControl'
import {useFocusEffect} from '@react-navigation/native'

const MembershipScreen = ({route, navigation}) => {
  const title = route.params.params?.page_name
  const membership = useSelector(state => state.infoServices.membershiplist)
  const internetState = useSelector(state => state.app.internetState)
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()

  const loadContent = useCallback(async () => {
    try {
      if (internetState) {
        await dispatch(fetchMembershipList())
      }
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch, internetState])

  useFocusEffect(
    useCallback(() => {
      if (internetState) {
        dispatch(setGlobalIndicatorVisibility(true))
        loadContent()
      }
    }, [internetState, loadContent, dispatch])
  )

  const showData = useMemo(() => {
    if (!membership?.length) {
      return []
    }
    return membership.sort((a, b) => a.listing.display_order - b.listing.display_order)
  }, [membership])

  const renderItem = itemData => {
    const listing = itemData.item.listing
    const cardDetail = new Card(listing.heading, listing.desc_short, 0, 0, 0, listing.image_banner)
    return (
      <View style={{marginBottom: responsiveHeight(5)}}>
        <CardItem
          cardDetail={cardDetail}
          titleStyle={{color: Colors().heroText}}
          onPress={() => {
            navigation.navigate(RouteKey.MembershipDetailsScreen, {listing})
          }}
        />
      </View>
    )
  }

  return (
    <ScreenContainer>
      <SubHeaderBar title={title} filterBtn={false} />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        ListEmptyComponent={<ListEmpty message={'No memberships found, please check again later.'} />}
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
      />
    </ScreenContainer>
  )
}

export default MembershipScreen
