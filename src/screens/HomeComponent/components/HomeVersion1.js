import React from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import Colors from '../../../Themes/Colors'
import {localize} from '../../../locale/I18nConfig'
import {FontSizes, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import {isTrue} from '../../../utilities/utils'
import StarButton from '../../../components/StarButton'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import Notification from '../../../components/Notification'
import MemberNameBar from '../../../components/MemberNameBar'
import HomeTilesBar from '../../../components/HomeTilesBar'
import PreferredVenueButton from '../../../components/PreferredVenueButton'
import BarcodeBar from '../../../components/BarcodeBar'
import {useSelector} from 'react-redux'
import GalleryComponent from './GalleryComponent'
import ComponentContainer from '../../../components/ComponentContainer'
import {Text} from '../../../components/Text'
import CustomRefreshControl from '../../../components/CustomRefreshControl'

function HomeVersion1({
  isRefreshing,
  internetState,
  setIsRefreshing,
  loadContent,
  galleries,
  profile,
  notifications,
  homeMenus,
  ifRoute
}) {
  const appFlags = useSelector(state => state.app.appFlags)
  return (
    <ScrollView
      refreshControl={
        <CustomRefreshControl
          refreshing={isRefreshing}
          onRefresh={() => {
            if (internetState) {
              setIsRefreshing(true)
              loadContent()
            }
          }}
        />
      }
    >
      <GalleryComponent showsPagination={false} height={responsiveHeight(300)} style={{marginBottom: 0}} />

      {isTrue(appFlags?.app_is_show_star_bar) && (
        <View style={styles.tiersContainer(profile?.member?.member_tier?.tier?.color)} />
      )}

      <ComponentContainer>
        {isTrue(appFlags?.app_is_show_star_bar) && (
          <StarButton
            style={{marginBottom: responsiveHeight(10)}}
            color={profile?.member?.member_tier?.tier?.color || Colors().heroText}
            onPress={() => navigate(RouteKey.CameraNavigator)}
          />
        )}
        {notifications?.length >= 1 && <Notification notifications={notifications} />}
        <MemberNameBar
          name={profile}
          onIconPress={() => {
            navigate(RouteKey.ProfileScreen)
          }}
          style={{marginTop: isTrue(appFlags?.app_is_show_star_bar) ? 0 : responsiveHeight(24)}}
        />
        {isTrue(appFlags?.app_show_tier_below_name) && (
          <Text centered>{profile?.member?.member_tier?.tier?.name}</Text>
        )}
        {isTrue(appFlags?.app_show_points_below_name) && (
          <Text style={{alignSelf: 'center'}}>POINTS BALANCE: {+profile?.member?.numberOf?.points}</Text>
        )}
        <HomeTilesBar
          homeMenus={homeMenus}
          userInfo={profile}
          onPress={ifRoute}
          marginVertical={responsiveHeight(30)}
        />
      </ComponentContainer>
      {!isTrue(appFlags?.app_is_show_preferred_venue) && (
        <View style={styles.singleTierBar(profile?.member?.member_tier?.tier?.color)}>
          {isTrue(appFlags?.app_show_tier_name) && (
            <Text fontSize={FontSizes.large} centered>
              {profile?.member?.member_tier?.tier?.name}
            </Text>
          )}
        </View>
      )}

      {isTrue(appFlags?.app_is_show_preferred_venue) && (
        <PreferredVenueButton
          style={{
            backgroundColor:
              isTrue(profile?.member?.current_preferred_venue_full?.is_preffered_venue_color) &&
              !!profile?.member?.current_preferred_venue_full?.color
                ? profile?.member?.current_preferred_venue_full?.color
                : Colors().cardFill
          }}
          icon="up"
          venueName={profile?.member?.current_preferred_venue_name}
          ifOnlineOrdering={profile?.member?.current_preferred_venue_full?.your_order_integration}
          onPress={() => {
            navigate(RouteKey.PreferredVenueScreen)
          }}
        />
      )}

      <BarcodeBar
        value={profile?.member?.bepoz_account_card_number}
        width={responsiveWidth(profile?.member?.bepoz_account_card_number?.length <= 10 ? 1.7 : 1.2)}
        title={localize('memberNumber')}
        text={profile?.member?.bepoz_account_number}
        style={styles.barCode}
      />
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {},
  tiersContainer: backgroundColor => ({
    width: '100%',
    height: responsiveHeight(37),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor
  }),
  barcode: {
    width: responsiveWidth(200),
    height: responsiveHeight(50),
    alignSelf: 'center'
  },
  textWrapper: {
    flex: 1,
    paddingHorizontal: responsiveWidth(2),
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  singleTierBar: backgroundColor => ({
    height: responsiveHeight(55),
    justifyContent: 'center',
    backgroundColor
  }),
  barCode: {
    paddingTop: responsiveHeight(30)
  }
})
export default HomeVersion1
