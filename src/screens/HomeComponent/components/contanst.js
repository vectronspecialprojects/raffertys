const SPECIAL_VIEW = 'special_view'
const TAB_VIEW = 'tab_view'
const SINGLE_VIEW = 'single_view'

export function getNumberData(data, userInfo) {
  let numberShow = ''
  const {member} = userInfo || {}
  switch (data?.page_layout) {
    case SPECIAL_VIEW:
      switch (data?.special_page) {
        case 'points':
          numberShow = `${+member?.points || 0} points`
          break
        case 'community-points':
          numberShow = `${member?.numberOf?.community_points || 0} points`
          break
        case 'saved':
          numberShow = member?.saved ? `$${member?.saved.toFixed(2)}` : '$0.00'
          break
        case 'balance':
          numberShow = member?.saved ? `$${member?.balance}` : '$0.00'
          break
        case 'tickets':
          numberShow = member?.numberOf?.tickets || 0
          break
        case 'vouchers':
          numberShow = member?.numberOf?.vouchers || 0
          break
        case 'favor':
          numberShow = member?.numberOf?.favorite || 0
          break
        case 'surveys':
          numberShow = member?.numberOf?.surveys || 0
          break
        default:
          numberShow = 0
          break
      }
      break
    case TAB_VIEW:
      switch (data?.pages[0]?.listing_type_id) {
        case '1':
        case '2':
          numberShow = member?.numberOf?.regularEvents + member?.numberOf?.specialEvents || 0
          break
        case '7':
        case '8':
          numberShow = member?.numberOf?.stampCard || 0
          break
      }
      break
    case SINGLE_VIEW:
      switch (data?.pages[0]?.listing_type_id) {
        case '4':
          numberShow = member?.numberOf?.promotions || 0
          break
        case '5':
          numberShow = member?.numberOf?.gifts || 0
          break
        case '6':
          numberShow = member?.numberOf?.ourTeam || 0
          break
        default:
          numberShow = 0
          break
      }
      break
  }
  return numberShow
}
