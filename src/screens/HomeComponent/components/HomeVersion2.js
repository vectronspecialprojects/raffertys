import React from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import {deviceWidth, responsiveHeight} from '../../../Themes/Metrics'
import {isTrue} from '../../../utilities/utils'
import MenuItem from './MenuItem'
import {getNumberData} from './contanst'
import CommunityDataComponent from './CommunityDataComponent'
import WhatOnHorizontalComponent from './WhatOnHorizontalComponent'
import {useSelector} from 'react-redux'
import LabelComponent from './LabelComponent'
import ButtonVenue from '../../../components/ButtonVenue'
import GalleryComponent from './GalleryComponent'
import CustomRefreshControl from '../../../components/CustomRefreshControl'
import Notification from '../../../components/Notification'

function HomeVersion2({
  isRefreshing,
  setIsRefreshing,
  loadContent,
  communityImpact,
  pledge,
  profile,
  internetState,
  homeMenus,
  ifRoute,
  notifications
}) {
  const pondHoppersEnable = useSelector(state => state.app.pondHoppersEnable)
  return (
    <View style={styles.container}>
      <ButtonVenue />
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <CustomRefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                if (internetState) {
                  setIsRefreshing(true)
                  loadContent()
                }
              }}
            />
          }
        >
          {notifications?.length >= 1 && <Notification notifications={notifications} />}
          {homeMenus?.map((item, index) => {
            switch (item.special_page) {
              case 'community-points':
                return (
                  <CommunityDataComponent
                    key={index.toString()}
                    communityImpact={communityImpact}
                    pledge={pledge}
                    item={item}
                    ifRoute={ifRoute}
                    profile={profile}
                  />
                )
              case 'points':
                return (
                  <MenuItem
                    key={index.toString()}
                    title={'Your membership'}
                    subTitle={isTrue(pondHoppersEnable) && 'Explore perks'}
                    actionSubTitle={'PERKS'} //Just hard code
                    style={{marginTop: responsiveHeight(15)}}
                    color={item.icon_color}
                    dataTitle={item.page_name}
                    value={getNumberData(item, profile)}
                    iconName={item.icon}
                    iconImage={item.image_icon}
                    valueRight={false}
                    disabled={item.state === 'none'}
                    onPress={() => ifRoute(item.id)}
                    iconSelector={item.icon_selector}
                  />
                )
              case 'what_on':
                return <WhatOnHorizontalComponent key={index.toString()} />
              case 'label':
                return <LabelComponent key={index.toString()} data={item} />
              case 'gallery':
                return <GalleryComponent key={index.toString()} height={deviceWidth() / 2.38} />
              default:
                return (
                  <MenuItem
                    key={index.toString()}
                    dataTitle={item.page_name}
                    value={getNumberData(item, profile)}
                    iconName={item.icon}
                    iconImage={item.image_icon}
                    onPress={() => ifRoute(item.id)}
                    color={item.icon_color}
                    disabled={item.state === 'none'}
                    iconSelector={item.icon_selector}
                  />
                )
            }
          })}
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
export default HomeVersion2
