import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {View, FlatList} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {fetchVouchers} from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import SubHeaderBar from './../components/SubHeaderBar'
import VoucherTile from './../components/VoucherTile'
import Filter from './../components/Filter'
import RouteKey from '../navigation/RouteKey'
import {Voucher} from '../modals/modals'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import ScreenContainer from '../components/ScreenContainer'
import ListEmpty from '../components/ListEmpty'
import CustomRefreshControl from '../components/CustomRefreshControl'

const VouchersScreen = props => {
  const title = props?.route?.params?.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isFilter, setIsFilter] = useState(false)
  const [selectedId, setSelectedId] = useState(0)
  const vouchers = useSelector(state => state.infoServices.vouchers)
  const profile = useSelector(state => state.infoServices.profile)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(fetchVouchers())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const filterHandler = venue => {
    setSelectedId(venue?.id)
    setIsFilter(false)
  }

  const showData = useMemo(() => {
    if (vouchers === undefined) {
      return []
    }
    if (vouchers !== undefined) {
      if (selectedId === 0) {
        return vouchers.sort((a, b) => {
          return a.id - b.id
        })
      }
      return vouchers
        .filter(data => data?.order_detail?.venue?.id === 0 || data?.order_detail?.venue?.id === selectedId)
        .sort((a, b) => {
          return a.id - b.id
        })
    }
  }, [vouchers, selectedId])

  const renderGridItem = itemData => {
    const voucherDetail = new Voucher(
      itemData?.item?.claim_promotion?.product?.name,
      itemData?.item?.claim_promotion?.product?.desc_short,
      itemData?.item?.barcode,
      itemData?.item?.issue_date,
      0,
      itemData?.item?.expire_date,
      itemData?.item?.voucher_type,
      itemData?.item?.amount_left,
      itemData?.item?.img,
      profile?.member?.bepoz_account_card_number,
      profile?.member?.bepoz_account_number
    )
    return (
      <VoucherTile
        imgSource={voucherDetail?.image}
        onPress={() => {
          props.navigation.navigate(RouteKey.VoucherDetailScreen, {voucherDetail})
        }}
        title={voucherDetail?.name}
        isInProgress={!voucherDetail?.barcode}
        expiredDate={voucherDetail?.expireDate}
      />
    )
  }

  return (
    <ScreenContainer>
      <SubHeaderBar
        title={title}
        filterBtn={false}
        onFilterPress={() => {
          setIsFilter(true)
        }}
      />
      <Filter
        isVisible={isFilter}
        backScreenOnPress={() => {
          setIsFilter(false)
        }}
        selectedId={selectedId}
        onFilterPress={filterHandler}
      />
      <View style={{flex: 1}}>
        <FlatList
          numColumns={2}
          columnWrapperStyle={{
            flex: 1,
            justifyContent: 'space-evenly'
          }}
          keyExtractor={item => item.id}
          data={showData}
          renderItem={renderGridItem}
          ListEmptyComponent={<ListEmpty message={localize('voucher.messageNotFound')} />}
          refreshControl={
            <CustomRefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                setIsRefreshing(true)
                loadContent()
              }}
            />
          }
        />
      </View>
    </ScreenContainer>
  )
}

export default VouchersScreen
