import React from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import Styles from '../../Themes/Styles'
import SubHeaderBar from '../../components/SubHeaderBar'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import BarcodeBar from '../../components/BarcodeBar'
import {localize} from '../../locale/I18nConfig'
import dayjs from 'dayjs'
import ScreenContainer from '../../components/ScreenContainer'
import {CardText} from '../../components/Text'

const TicketType = ['1']

function TicketDetailScreen({route}) {
  const {ticketDetail} = route.params

  return (
    <ScreenContainer>
      <SubHeaderBar title={localize('ticket.title')} />
      <View style={{flex: 1, paddingHorizontal: responsiveWidth(20)}}>
        <ScrollView>
          <CardText
            style={{
              ...Styles.largeCapBoldText,
              ...{marginTop: responsiveHeight(30)}
            }}
          >
            {ticketDetail.name}
          </CardText>
          {ticketDetail.description && (
            <View style={styles.descContainer}>
              <CardText style={Styles.smallCapText}>{ticketDetail.description}</CardText>
            </View>
          )}
          {!!ticketDetail.barcode && (
            <BarcodeBar
              value={ticketDetail.barcode}
              width={responsiveWidth(1.5)}
              title={localize('ticket.lookupNumber')}
              text={ticketDetail.barcode}
              style={{marginTop: responsiveHeight(30)}}
            />
          )}
          <View style={styles.ticketInfoContainer}>
            {ticketDetail.issue_date && (
              <CardText style={Styles.xSmallCapText}>
                Issue Date: {dayjs(ticketDetail.issue_date).format('DD/MM/YYYY')}
              </CardText>
            )}
            {ticketDetail.expire_date && (
              <CardText style={Styles.xSmallCapText}>
                Expiry Date: {dayjs(ticketDetail.expire_date).format('DD/MM/YYYY')}
              </CardText>
            )}
            {TicketType.includes(ticketDetail?.voucher_type) && (
              <CardText style={Styles.xSmallCapText}>
                Remaining Value:{' '}
                {ticketDetail.amount_left
                  ? '$0'
                  : (ticketDetail.amount_left / 100).toLocaleString('en-US', {
                      style: 'currency',
                      currency: 'USD'
                    })}
              </CardText>
            )}
          </View>
          {!!ticketDetail?.member_barcode && (
            <BarcodeBar
              value={ticketDetail.member_barcode}
              width={1.5}
              title={localize('memberNumber')}
              text={ticketDetail.member_barcode}
              style={{marginTop: responsiveHeight(100)}}
            />
          )}
        </ScrollView>
      </View>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  ticketInfoContainer: {
    marginTop: responsiveHeight(50)
  },
  descContainer: {
    marginTop: responsiveHeight(30)
  }
})

export default TicketDetailScreen
