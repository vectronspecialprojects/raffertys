import React, {useState, useEffect, useCallback} from 'react'
import {View, ScrollView, StyleSheet, LayoutAnimation} from 'react-native'
import {onLinkPress, TouchableCmp} from '../components/UtilityFunctions'
import {useSelector, useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import * as infoServicesActions from '../store/actions/infoServices'
import Html from '../components/Html'
import SubHeaderBar from './../components/SubHeaderBar'
import Colors from '../Themes/Colors'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Fonts from '../Themes/Fonts'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import {isTrue} from '../utilities/utils'
import {navigate} from '../navigation/NavigationService'
import RouteKey from '../navigation/RouteKey'
import ScreenContainer from '../components/ScreenContainer'
import {CardText} from '../components/Text'
import CustomRefreshControl from '../components/CustomRefreshControl'
import ComponentContainer from '../components/ComponentContainer'
import Metrics from '../Themes/Metrics'

const LegalsScreen = props => {
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isGroupShown, setIsGroupShown] = useState('')
  const legals = useSelector(state => state.infoServices.legals)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchLegals)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  function onItemPress(item) {
    if (isTrue(item.webview_enable)) {
      navigate(RouteKey.WebviewScreen, {
        params: {
          appUri: item.webview_url
        }
      })
    } else {
      if (isGroupShown === item.title) {
        setIsGroupShown('')
      } else {
        setIsGroupShown(item.title)
      }
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    }
  }

  function renderItem(item, index) {
    const isSelected = isGroupShown === item?.title
    return (
      <ComponentContainer style={styles.item} key={index.toString()}>
        <TouchableCmp activeOpacity={0.6} onPress={() => onItemPress(item)}>
          <View style={styles.titleContainer}>
            <CardText style={styles.title}>{item?.title}</CardText>
            <AntDesign name={isSelected ? 'down' : 'right'} size={22} color={Colors().cardText} />
          </View>
        </TouchableCmp>
        {isSelected && (
          <Html
            html={item?.content || ''}
            textAlign={'left'}
            color={Colors().cardText}
            onLinkPress={onLinkPress}
          />
        )}
      </ComponentContainer>
    )
  }

  return (
    <ScreenContainer>
      <SubHeaderBar title={title} />
      <ScrollView
        refreshControl={
          <CustomRefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
          />
        }
      >
        {legals.map(renderItem)}
      </ScrollView>
    </ScreenContainer>
  )
}

const styles = StyleSheet.create({
  titleContainer: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: 20,
    textAlign: 'left'
  },
  item: {
    marginHorizontal: Metrics.small
  }
})

export default LegalsScreen
