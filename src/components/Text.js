import React from 'react'
import {Text as RNText, StyleSheet} from 'react-native'
import Fonts from '../Themes/Fonts'
import Metrics, {FontSizes} from '../Themes/Metrics'
import Colors from '../Themes/Colors'

export const Text = ({color, fontSize = FontSizes.span, children, style, centered, ...rest}) => {
  return (
    <RNText {...rest} style={[styles.textNormal(color || Colors().heroText, fontSize, centered), style]}>
      {children}
    </RNText>
  )
}

export const TextBold = ({...rest}) => {
  return <Text {...rest} style={styles.textBold} />
}

export const Link = ({style, ...rest}) => {
  return <Text {...rest} style={[styles.link, style]} color={Colors().linkText} />
}

export const HeaderTitle = ({style, ...rest}) => {
  return <Text {...rest} style={[styles.header, style]} color={Colors().heroText} fontSize={Metrics.title} />
}

export const CardText = ({color, ...rest}) => {
  return <Text {...rest} color={color || Colors().cardText} />
}

export const BackgroundText = rest => {
  return <Text {...rest} color={Colors().backgroundText} />
}

export const HeroText = rest => {
  return <Text {...rest} color={Colors().heroText} />
}

export const DialogText = rest => {
  return <Text {...rest} color={Colors().alertDialogText} />
}

export const ErrorText = rest => {
  return <Text {...rest} color={Colors().errorText} />
}

const styles = StyleSheet.create({
  textNormal: (color, fontSize, centered) => ({
    color,
    fontSize: fontSize || FontSizes.span,
    fontFamily: Fonts.regular,
    textAlign: centered ? 'center' : 'left'
  }),
  textBold: {
    fontFamily: Fonts.bold
  },
  link: {},
  header: {
    fontFamily: Fonts.bold
  }
})
