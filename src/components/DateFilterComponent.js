import React, {useEffect, useState} from 'react'
import {View, StyleSheet} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import DateTime from './DateTime'
import dayjs from 'dayjs'
import Fonts from '../Themes/Fonts'
import {HeroText} from './Text'

function DateFilterComponent({onChange}) {
  const [dateFrom, setDateFrom] = useState('')
  const [dateTo, setDateTo] = useState('')

  useEffect(() => {
    onChange({
      from: dateFrom ? dayjs(dateFrom).format('YYYY-MM-DD') : '',
      to: dateTo ? `${dayjs(dateTo).format('YYYY-MM-DD')} 23:59:59` : ''
    })
    /*eslint-disable react-hooks/exhaustive-deps*/
  }, [dateFrom, dateTo])

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.buttonDate}>
          <HeroText style={styles.textTitle}>From</HeroText>
          <DateTime
            maxDate={dateTo}
            value={dateFrom}
            onChange={date => {
              setDateFrom(date)
            }}
          />
        </View>
        <View style={styles.buttonDate}>
          <HeroText style={styles.textTitle}>To</HeroText>
          <DateTime
            minDate={dateFrom}
            value={dateTo}
            onChange={date => {
              setDateTo(date)
            }}
          />
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: responsiveWidth(12),
    paddingVertical: responsiveHeight(20)
  },
  buttonDate: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textTitle: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans
  }
})

export default DateFilterComponent
