import React from 'react'
import {View, StyleSheet} from 'react-native'
import {responsiveWidth, shadow} from '../Themes/Metrics'
import HomeTile from './HomeTile'
import Colors from '../Themes/Colors'
import {getNumberData} from '../screens/HomeComponent/components/contanst'
import {isTrue} from '../utilities/utils'

const HomeTilesBar = props => {
  return (
    <View style={{...styles.tilesContainer, ...{marginVertical: props.marginVertical}}}>
      {props?.homeMenus.map(homeMenu => {
        if (homeMenu?.page_name) {
          let numberShow = getNumberData(homeMenu, props?.userInfo)
          return (
            <HomeTile
              style={{
                backgroundColor:
                  isTrue(props?.userInfo?.member?.current_preferred_venue_full?.is_preffered_venue_color) &&
                  !!props?.userInfo?.member?.current_preferred_venue_full?.color
                    ? props?.userInfo?.member?.current_preferred_venue_full?.color
                    : Colors().cardFill
              }}
              key={homeMenu?.id}
              title={homeMenu?.page_name}
              icon={homeMenu?.icon}
              numberShow={numberShow}
              onPress={props?.onPress.bind(this, homeMenu.id)}
              imageIcon={homeMenu.image_icon}
            />
          )
        }
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  tilesContainer: {
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: responsiveWidth(20),
    ...shadow
  }
})

export default HomeTilesBar
