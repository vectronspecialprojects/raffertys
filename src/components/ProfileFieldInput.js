import React, {useMemo, useState} from 'react'
import {View} from 'react-native'
import TextInputView from './TextInputView'
import {localize} from '../locale/I18nConfig'
import Colors from '../Themes/Colors'
import Dropdown from './Dropdown'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import DateInputView from './DateInputView'
import {useSelector} from 'react-redux'
import {isEmptyValues} from '../utilities/utils'
import DropDownList from './DropDownList'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

function ProfileFieldInput({
  formState,
  type,
  inputChangeHandler,
  listTier,
  item,
  value,
  onPress,
  hasConfirmPassword = true,
  editable = true,
  checkInitialEmail = false,
  isFilterVenues = false
}) {
  const venues = useSelector(state => state.infoServices?.[isFilterVenues ? 'filterVenues' : 'venues'])
  const emailNotAllowedDomains = useSelector(state => state.app.emailNotAllowedDomains)
  const [showSelectVenue, setShowSelectVenue] = useState(false)

  const dropDownItems = useMemo(() => {
    const items = []
    let data = venues
    for (const key in data) {
      items.push({label: data[key].name, value: data[key].id})
    }
    return items
  }, [venues])

  const dropDownTier = useMemo(() => {
    if (!listTier?.length) {
      return []
    }
    const list = []
    listTier?.map(item => {
      list.push({
        label: item?.name,
        value: item?.id
      })
    })
    return list
  }, [listTier])

  switch (type) {
    case 'Venue':
      return (
        <>
          <TextInputView
            title={item?.fieldDisplayTitle}
            onPress={() => setShowSelectVenue(true)}
            placeholder={localize('signUp.preferredVenuePlaceholder')}
            editable={false}
            backgroundDisabled={Colors().formInputFieldsFill}
            value={formState?.inputValues?.venue_id?.label || value?.label}
            required={item.required}
            rightIcon={
              <MaterialCommunityIcons
                name={'chevron-down'}
                size={25}
                color={Colors().formInputFieldsText}
                style={{marginRight: responsiveWidth(10)}}
              />
            }
          />
          <DropDownList
            data={dropDownItems}
            visible={showSelectVenue}
            hasSearchBar={true}
            modalTitle={localize('signUp.preferredVenue')}
            height={responsiveHeight(350)}
            showValue={'label'}
            onClose={() => setShowSelectVenue(false)}
            value={formState?.inputValues?.venue_id?.value || value?.value}
            placeholder={localize('signUp.preferredVenuePlaceholder')}
            onSelect={(venue, isValid) => {
              inputChangeHandler('venue_id', venue, isValid)
            }}
          />
        </>
      )
    case 'Tier':
      return (
        <Dropdown
          id="tier"
          required={item.required}
          onInputChange={inputChangeHandler}
          label={localize('signUp.tier')}
          placeholder={localize('signUp.tierPlaceholder')}
          items={dropDownTier}
          initValue={formState.inputValues?.[item.id] || value}
        />
      )
    case 'Dropdown':
      return (
        <Dropdown
          id={item?.id}
          placeholder={item?.fieldDisplayTitle}
          required={item.required}
          onInputChange={inputChangeHandler}
          label={item?.fieldDisplayTitle}
          items={customFields?.(item?.multiValues)}
          initValue={formState?.inputValues?.[item.id] || value}
        />
      )
    case 'Text':
    case 'Number':
      return (
        <TextInputView
          alphabet={!item?.id?.includes('email')}
          id={item?.id}
          title={item?.fieldDisplayTitle}
          keyboardType={item.keyboardType}
          required={item.required}
          email={item?.id?.includes('email')}
          emailAvailability={item?.id?.includes('email')}
          checkInitialEmail={item?.id?.includes('email') && checkInitialEmail}
          emailNotAllowedDomains={item?.id?.includes('email') ? emailNotAllowedDomains : null}
          autoCapitalize="words"
          placeholder={item?.fieldDisplayTitle}
          onChangeText={inputChangeHandler}
          initialValue=""
          errorText={`Please enter a valid ${item?.fieldDisplayTitle?.toLowerCase()}.`}
          value={formState?.inputValues?.[item?.id] || value}
          minLength={+item?.minLength || 0}
          maxLength={+item?.maxLength || 999}
          editable={editable}
          onPress={onPress}
        />
      )
    case 'Password':
      return (
        <View>
          <TextInputView
            id="password"
            title={localize('signUp.password')}
            placeholder={localize('signUp.password')}
            keyboardType="default"
            secureTextEntry
            required
            password
            autoCapitalize="none"
            errorText={localize('signUp.errPassword')}
            onChangeText={inputChangeHandler}
            initialValue=""
            value={formState?.inputValues?.password || value}
            editable={editable}
            onPress={onPress}
          />
          {hasConfirmPassword && (
            <TextInputView
              id="password_confirmation"
              title={localize('signUp.confirmPassword')}
              placeholder={localize('signUp.confirmPassword')}
              keyboardType="default"
              secureTextEntry
              required
              resetEvent={formState?.inputValues?.password?.length <= 1}
              confirmPassword
              crossCheckPassword={formState?.inputValues?.password}
              autoCapitalize="none"
              errorText={localize('signUp.confirmPassErr')}
              onChangeText={inputChangeHandler}
              initialValue=""
              value={formState?.inputValues?.password_confirmation || value}
              editable={editable}
              onPress={onPress}
            />
          )}
        </View>
      )
    case 'Date':
      return (
        <DateInputView
          id={item?.id}
          initialDate={formState?.inputValues?.dob || value}
          required={item.required}
          onInputChange={inputChangeHandler}
          label={item?.fieldDisplayTitle}
          style={{
            backgroundColor: Colors().formInputFieldsFill
          }}
          errorText={`Please enter a valid ${item?.fieldDisplayTitle?.toLowerCase()}.`}
          editable={editable}
        />
      )
    default:
      return (
        <TextInputView
          alphabet={!item?.id?.includes('email')}
          id={item?.id}
          title={item?.fieldDisplayTitle}
          keyboardType={item?.keyboardType}
          required={item.required}
          email={item?.id?.includes('email')}
          checkInitialEmail={item?.id?.includes('email') && checkInitialEmail}
          emailAvailability={item?.id?.includes('email')}
          emailNotAllowedDomains={item?.id?.includes('email') ? emailNotAllowedDomains : null}
          autoCapitalize="words"
          placeholder={item?.fieldDisplayTitle}
          onChangeText={inputChangeHandler}
          initialValue=""
          errorText={`Please enter a valid ${item?.fieldDisplayTitle?.toLowerCase()}.`}
          value={formState?.inputValues?.[item?.id] || value}
          minLength={+item?.minLength || 0}
          maxLength={+item?.maxLength || 999}
          editable={editable}
          onPress={onPress}
        />
      )
  }
}

const customFields = values => {
  let list = []
  if (Array.isArray(values)) {
    values.map(item => {
      list.push({
        label: item?.fieldName || item,
        value: item?.id || item
      })
    })
  } else {
    if (isEmptyValues(values)) {
      return []
    }
    Object.values(values).map(item => {
      list.push({
        label: item?.fieldName || item,
        value: item?.id || item
      })
    })
  }

  return list
}

export default ProfileFieldInput
