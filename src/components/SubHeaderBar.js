import React, {useMemo} from 'react'
import {View, StyleSheet} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'
import Metrics, {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {useSelector} from 'react-redux'
import {isTrue} from '../utilities/utils'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import ButtonView from './ButtonView'
import {HeaderTitle} from './Text'
import {HeroComponent} from './ComponentContainer'

const SubHeaderBar = ({filterBtn, title, selectedVenueID, onFilterPress, style}) => {
  const appFlags = useSelector(state => state.app.appFlags)
  const venues = useSelector(state => state.infoServices.filterVenues)

  const selectedVenue = useMemo(() => {
    return venues.find(i => i.id === selectedVenueID)
  }, [selectedVenueID, venues])

  const showFilter = useMemo(() => {
    return filterBtn && isTrue(appFlags?.isMultipleVenue)
  }, [appFlags, filterBtn])

  if (isTrue(appFlags?.app_sub_header_align_left)) {
    return (
      <HeroComponent style={styles.container}>
        <HeaderTitle>{title}</HeaderTitle>
        {showFilter && (
          <View style={[styles.dropdownContainer, {borderColor: Colors().heroFill}]}>
            <ButtonView
              onPress={onFilterPress}
              title={selectedVenue?.name || 'Venue name'}
              style={[styles.venueButton]}
              titleStyle={{color: Colors().heroText}}
              rightIcon={<MaterialCommunityIcons name={'chevron-down'} size={22} color={Colors().linkText} />}
              titleContainerStyle={styles.titleContainer}
              hasIcon
            />
          </View>
        )}
      </HeroComponent>
    )
  }

  return (
    <HeroComponent style={[styles.container, styles.headerCenter, style]}>
      <HeaderTitle>{title}</HeaderTitle>
      {showFilter && (
        <View style={styles.iconContainer}>
          <TouchableCmp onPress={onFilterPress} style={{flex: 1}}>
            <View style={styles.wrapper}>
              <FontAwesome5Pro
                name="filter"
                size={responsiveFont(15)}
                color={Colors().linkText}
                activeOpacity={0.6}
              />
            </View>
          </TouchableCmp>
        </View>
      )}
    </HeroComponent>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: responsiveHeight(44),
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: Metrics.medium
  },
  headerCenter: {
    justifyContent: 'center'
  },
  iconContainer: {
    position: 'absolute',
    right: responsiveWidth(15),
    borderRadius: 5,
    borderWidth: 0,
    overflow: 'hidden'
  },
  wrapper: {
    padding: 5
  },
  title: color => ({
    fontSize: responsiveFont(20),
    fontFamily: Fonts.openSansBold,
    marginTop: responsiveHeight(5),
    marginLeft: responsiveHeight(20),
    color
  }),
  venueButton: {
    height: responsiveHeight(38),
    borderRadius: responsiveHeight(19),
    width: responsiveWidth(160),
    paddingRight: responsiveWidth(5)
  },
  titleContainer: {
    paddingHorizontal: responsiveWidth(10)
  },
  dropdownContainer: {
    position: 'absolute',
    right: 0,
    overflow: 'hidden'
  }
})

export default SubHeaderBar
