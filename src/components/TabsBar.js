import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {responsiveHeight, shadow} from '../Themes/Metrics'

const Tab = ({selectedPageId, page, onPress}) => {
  const isSelected = +selectedPageId === +page?.listing_type_id
  return (
    <View style={styles.tabContainer(isSelected ? Colors().activeTabFill : Colors().inactiveTabFill)}>
      <TouchableCmp activeOpacity={0.6} style={{flex: 1}} onPress={() => onPress(page?.listing_type_id)}>
        <View style={styles.textContainer}>
          <Text
            style={styles.title(isSelected ? Colors().activeTabText : Colors().inactiveTabText)}
            numberOfLines={1}
          >
            {page?.title}
          </Text>
        </View>
      </TouchableCmp>
    </View>
  )
}

const TabsBar = props => {
  const pages = props.pages
  return (
    <View style={[styles.bar(Colors().backgroundFill), props.isShadow && {...shadow}]}>
      {pages?.map(page => {
        return (
          <Tab
            selectedPageId={props.selectedPageId}
            onPress={props.onPress}
            page={page}
            key={page.listing_type_id}
          />
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  bar: backgroundColor => ({
    width: '100%',
    flexDirection: 'row',
    height: responsiveHeight(44),
    backgroundColor
  }),
  textContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: color => ({
    fontFamily: Fonts.openSansBold,
    fontSize: 14,
    color
  }),
  tabContainer: backgroundColor => ({
    backgroundColor,
    flex: 1
  })
})

export default TabsBar
