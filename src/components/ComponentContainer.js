import React from 'react'
import {StyleSheet, View} from 'react-native'
import Colors from '../Themes/Colors'

const ComponentContainer = ({style, backgroundColor, children, ...rest}) => {
  return (
    <View {...rest} style={[styles.container(backgroundColor || Colors().backgroundFill), style]}>
      {children}
    </View>
  )
}

export const HeroComponent = ({style, children, ...rest}) => {
  return (
    <View {...rest} style={[styles.container(Colors().heroFill), style]}>
      {children}
    </View>
  )
}

export const DialogComponent = ({style, children, ...rest}) => {
  return (
    <View {...rest} style={[styles.container(Colors().alertDialogFill), style]}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  container: backgroundColor => ({
    backgroundColor
  })
})

export default ComponentContainer
