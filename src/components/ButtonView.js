import {StyleSheet, Text, View} from 'react-native'
import React from 'react'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {TouchableCmp} from './UtilityFunctions'
import {responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../Themes/Metrics'
import {useSelector} from 'react-redux'

export default function ButtonView({
  title,
  desc,
  style,
  onPress,
  disabled,
  hasIcon,
  titleStyle,
  descStyle,
  children,
  wrapper,
  leftIcon,
  rightIcon,
  isShadow,
  titleContainerStyle,
  backgroundColor,
  borderColor,
  textColor
}) {
  const formInputStyle = useSelector(state => state.app.formInputStyle)
  const useLegacyDesign = useSelector(state => state.app.useLegacyDesign)
  return (
    <View
      style={[
        styles.container(
          backgroundColor || Colors().cardFill,
          isShadow,
          disabled,
          useLegacyDesign,
          borderColor
        ),
        styles[formInputStyle],
        style
      ]}
    >
      <TouchableCmp style={{flex: 1}} disabled={disabled} onPress={onPress}>
        <View style={[styles.textWrapper, wrapper]}>
          {!!leftIcon && leftIcon}
          <View style={[{flex: 1, paddingHorizontal: responsiveWidth(20)}, titleContainerStyle]}>
            {!!title && (
              <Text
                style={[
                  styles.titleStyle(textColor || Colors().cardText),
                  !hasIcon && {textAlign: 'center'},
                  titleStyle
                ]}
              >
                {title}
              </Text>
            )}
            {desc && (
              <Text
                style={[
                  styles.descStyle(textColor),
                  {color: Colors().cardText},
                  !hasIcon && {textAlign: 'center'},
                  descStyle
                ]}
              >
                {desc}
              </Text>
            )}
          </View>
          {!!rightIcon && rightIcon}
          {children}
        </View>
      </TouchableCmp>
    </View>
  )
}

export const BorderButton = ({borderColor, ...rest}) => {
  return (
    <ButtonView
      {...rest}
      backgroundColor={'transparent'}
      borderColor={borderColor || Colors().signupButtonBorder}
    />
  )
}

export const NegativeButton = ({...rest}) => {
  return (
    <ButtonView
      {...rest}
      backgroundColor={Colors().cancelButtonFill}
      borderColor={Colors().cancelButtonBorder}
      textColor={Colors().cancelButtonText}
    />
  )
}

export const PositiveButton = ({...rest}) => {
  return (
    <ButtonView
      {...rest}
      backgroundColor={Colors().okButtonFill}
      borderColor={Colors().okButtonBorder}
      textColor={Colors().okButtonText}
    />
  )
}

const styles = StyleSheet.create({
  container: (backgroundColor, isShadow, disabled, useLegacyDesign, borderColor) => {
    let style = {
      height: responsiveHeight(40),
      borderRadius: responsiveHeight(20),
      overflow: 'hidden',
      backgroundColor,
      opacity: disabled ? 0.5 : 1,
      borderWidth: 2,
      borderColor: borderColor || backgroundColor
    }
    if (isShadow) {
      style = {
        ...style,
        ...shadow
      }
    }
    if (useLegacyDesign) {
      style = {
        ...style,
        height: responsiveHeight(44),
        borderRadius: responsiveHeight(22)
      }
    }
    return style
  },
  rounded: {
    borderRadius: responsiveHeight(19)
  },
  rectangle: {
    borderRadius: responsiveHeight(3)
  },
  textWrapper: {
    flex: 1,
    height: responsiveHeight(38),
    borderRadius: 12,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  wrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: responsiveWidth(16)
  },
  titleStyle: color => ({
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(15),
    color
  }),
  descStyle: color => ({
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(10),
    color
  })
})
