import React from 'react'
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderButton from './HeaderButton'
import Styles from '../Themes/Styles'
import {isIOS} from '../Themes/Metrics'

function HeaderLeftButton({navData, iconName, onPress}) {
  const {route, navigation} = navData || {}

  if (route?.params?.params?.hideHeaderLeft) {
    return null
  }
  return (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        style={Styles.drawer}
        title="Menu"
        iconName={iconName ? iconName : !isIOS() ? 'md-arrow-back' : 'ios-arrow-back'}
        onPress={() => {
          if (onPress) {
            onPress()
          } else {
            navigation.goBack()
          }
        }}
      />
    </HeaderButtons>
  )
}

export default HeaderLeftButton
