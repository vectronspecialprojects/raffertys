import React from 'react'
import {StyleSheet} from 'react-native'
import ComponentContainer from './ComponentContainer'

const SeparatorComponent = () => {
  return <ComponentContainer style={{height: StyleSheet.hairlineWidth}} />
}

export default SeparatorComponent
