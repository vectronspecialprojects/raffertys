import React from 'react'
import {StyleSheet, View} from 'react-native'
import Colors from '../Themes/Colors'

const ScreenContainer = ({children}) => {
  return <View style={styles.container(Colors().backgroundFill)}>{children}</View>
}

const styles = StyleSheet.create({
  container: backgroundColor => ({
    backgroundColor,
    flex: 1
  })
})

export default ScreenContainer
