import React from 'react'
import {ImageBackground, View, StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Styles from '../Themes/Styles'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import dayjs from 'dayjs'
import {CardText, HeroText} from './Text'

const VoucherTile = props => {
  return (
    <ImageBackground
      style={styles.gridItem}
      resizeMode="contain"
      source={{uri: props.imgSource || 'https://s3-ap-southeast-2.amazonaws.com/bepoz-loyalty-app/000.png'}}
    >
      <TouchableCmp
        activeOpacity={0.6}
        style={{flex: 1}}
        onPress={props.onPress}
        disabled={props?.isInProgress}
      >
        <View style={[styles.container, {backgroundColor: Colors().opacity}]}>
          <HeroText style={Styles.largeCapBoldText}>{props.title}</HeroText>
          {props.isInProgress && <CardText style={Styles.mediumCapText}>In Progress</CardText>}
          {props.expiredDate && (
            <View style={styles.expiredContainer}>
              <HeroText style={[Styles.x2SmallNormalText, {alignSelf: 'flex-start'}]}>
                {dayjs(props.expiredDate).format('DD/MM/YYYY')}
              </HeroText>
            </View>
          )}
        </View>
      </TouchableCmp>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  gridItem: {
    width: responsiveWidth(150),
    aspectRatio: 1,
    marginTop: responsiveHeight(20),
    borderRadius: responsiveWidth(5),
    overflow: 'hidden'
  },
  container: {
    flex: 1,
    borderRadius: responsiveWidth(5),
    paddingHorizontal: responsiveWidth(10),
    paddingTop: '30%',
    alignItems: 'center'
  },
  expiredContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    width: '100%',
    paddingBottom: responsiveHeight(5)
  }
})

export default VoucherTile
