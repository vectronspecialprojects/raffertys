import React from 'react'
import {View, StyleSheet} from 'react-native'
import {responsiveHeight} from '../Themes/Metrics'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {CardText} from './Text'

const DateBar = props => {
  return (
    <View style={{...styles.bar, ...props.style}}>
      {props.days?.map(day => (
        <View
          key={day.order}
          style={[Styles.dayFrame, {borderColor: Colors().cardText}, {borderColor: props.color}]}
        >
          <CardText style={{...Styles.xSmallUpText, ...{color: props.color}}}>
            {day.name.substr(0, 3)}
          </CardText>
        </View>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  bar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: responsiveHeight(10)
  }
})

export default DateBar
