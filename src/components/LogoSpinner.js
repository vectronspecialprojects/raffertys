import React, {useEffect, useRef} from 'react'
import {Animated, StyleSheet, Easing} from 'react-native'
import {logoImage} from '../constants/constants'
import Colors from '../Themes/Colors'
import FastImage from 'react-native-fast-image'

const LogoSpinner = ({size, style, ...rest}) => {
  const rotateAnimate = useRef(new Animated.Value(0)).current

  useEffect(() => {
    Animated.loop(
      Animated.timing(rotateAnimate, {
        toValue: 1,
        duration: 3000,
        useNativeDriver: true,
        easing: Easing.linear
      })
    ).start()
  }, [rotateAnimate])

  const rotate = rotateAnimate.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg']
  })

  return (
    <Animated.View
      style={[
        styles.spinner(size, Colors().heroFill),
        {
          transform: [{rotate: rotate}]
        }
      ]}
    >
      <FastImage source={{uri: logoImage}} style={styles.image(size)} resizeMode={'contain'} />
    </Animated.View>
  )
}

export default LogoSpinner

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  spinner: (size, backgroundColor) => ({
    borderRadius: size / 2,
    height: size,
    width: size,
    position: 'absolute'
  }),
  image: size => ({
    width: '100%',
    height: size / 5
  })
})
