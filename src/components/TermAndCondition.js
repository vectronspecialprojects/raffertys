import React, {useState} from 'react'
import {View, StyleSheet} from 'react-native'
import Styles from '../Themes/Styles'
import {useSelector} from 'react-redux'
import HtmlTextPopup from './HtmlTextPopup'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import CheckBox from './CheckBox'
import Fonts from '../Themes/Fonts'
import {isTrue} from '../utilities/utils'
import {navigate} from '../navigation/NavigationService'
import RouteKey from '../navigation/RouteKey'
import {BackgroundText, Link} from './Text'
import {localize} from '../locale/I18nConfig'

function TermAndCondition({onCheck, value, allowCheck}) {
  const [showData, setShowData] = useState({header: '', html: ''})
  const legals = useSelector(state => state.infoServices.legals)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const appFlags = useSelector(state => state.app.appFlags)

  const legalsPopup = cases => {
    let item
    if (cases === 'termAndCondition') {
      item = legals[1]
    } else if (cases === 'policy') {
      item = legals[0]
    }
    if (isTrue(item.webview_enable)) {
      navigate(RouteKey.WebviewScreen, {
        params: {
          appUri: item.webview_url
        }
      })
    } else {
      setIsPopupVisible(true)
      setShowData({header: item?.title, html: item?.content})
    }
  }

  return (
    <View style={styles.container}>
      {allowCheck && <CheckBox size={responsiveHeight(35)} value={value} onPress={onCheck} />}
      <View style={[styles.termContainer, !allowCheck && {paddingHorizontal: responsiveWidth(20)}]}>
        <BackgroundText style={[Styles.xSmallNormalText, {textAlign: 'left'}]}>
          {localize('terms.firstPhrase', {appName: appFlags?.app_name})}
          <Link
            onPress={() => {
              legalsPopup('termAndCondition')
            }}
            style={{fontFamily: Fonts.bold}}
          >
            {localize('terms.termAndConditions')}
          </Link>{' '}
          and{' '}
          <Link
            onPress={() => {
              legalsPopup('policy')
            }}
            style={{fontFamily: Fonts.bold}}
          >
            {localize('terms.privacyPolicy')}
          </Link>
          {localize('terms.lastPhrase')}
        </BackgroundText>
      </View>
      <HtmlTextPopup
        isVisible={isPopupVisible}
        header={showData.header}
        html={showData.html}
        onOkPress={() => setIsPopupVisible(false)}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  termContainer: {
    alignItems: 'center',
    paddingRight: responsiveWidth(20)
  }
})

export default TermAndCondition
