export class Voucher {
  constructor(
    name,
    description,
    barcode,
    issueDate,
    price,
    expireDate,
    voucherType,
    amountLeft,
    image,
    memberBarcode,
    memberNumber
  ) {
    this.name = name
    this.description = description
    this.barcode = barcode
    this.issueDate = issueDate
    this.price = price
    this.expireDate = expireDate
    this.voucherType = voucherType
    this.amountLeft = amountLeft
    this.image = image
    this.memberBarcode = memberBarcode
    this.memberNumber = memberNumber
  }
}

export class Card {
  constructor(title, description, eventDays, emptyStars, filledStarts, image) {
    this.title = title
    this.description = description
    this.eventDays = eventDays
    this.emptyStars = emptyStars
    this.filledStarts = filledStarts
    this.image = image
  }
}
