import {Platform} from 'react-native'

const env = {
  production: 'production',
  staging: 'staging'
}

const appSecret = '2da489f7ac6cce34ddcda02ef126270b'
const appToken = '12a3e1a732ee'

export const buildEvn = env.production

const baseUrl = {
  staging: 'https://api2.vecport.net/raffertystavern/api/',
  production: 'https://api2.vecport.net/raffertystavern/api/'
}

export const codePushKey = Platform.select({
  ios: {
    staging: 'PndPvC-0BQX0-YKjkaXyW11D0W345IIY21iJy',
    production: 'lopULcyeUyqDO_x_vgc_DJffcxxga47QCX0pP',
  },
  android: {
    staging: 'uwgFXkiufDHMs1jg8y4ShZME9MWr2Wj2UazNT',
    production: 'DhbJG1RxLO_FvEWwP9PwDEqyznXePz8qW5dk7',
  }
})

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: '0b6a4101-5204-426e-a3d2-feaf455058f7',
  buildEvn: buildEvn,
  codePushKey: codePushKey[buildEvn],
  baseUrl: baseUrl[buildEvn],
  appSecret,
  appToken
}

export default vars
